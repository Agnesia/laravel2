<!DOCTYPE html>
<html>
	<head>
		<title>Form Sign Up</title>
	</head>
	<body>
		<h1>Buat Account Baru!</h1>
		<h3>Sign Up Form</h2>
		 <form action="/welcome" method="post">
			{{ csrf_field() }}
  			<label for="fname">First name:</label><br><br>
  				<input type="text" id="fname" name="fname"><br><br>
  			<label for="lname">Last name:</label><br><br>
  			<input type="text" id="lname" name="lname"><br><br>
			<label for="gender">Gender:</label><br><br>
 			<input type="radio" id="male" name="gender" value="male">
  			<label for="male">Male</label><br>
  			<input type="radio" id="female" name="gender" value="female">
  			<label for="female">Female</label><br>
  			<input type="radio" id="other" name="gender" value="other">
  			<label for="other">Other</label><br><br>
			<label for="nationality">Nationality:</label><br><br>
			<select id="nationality" name="nationality">
  				<option value="indonesia">Indonesia</option>
			</select><br><br>
			<label for="lang">Language Spoken:</label><br><br>
			<input type="checkbox" id="lang-spoken1" name="lang-spoken1" value="ina">
				<label for="lang-spoken1">Bahasa Indonesia</label><br>
			<input type="checkbox" id="lang-spoken2" name="lang-spoken2" value="eng">			
				<label for="lang-spoken2">English</label><br>
				<input type="checkbox" id="lang-spoken3" name="lang-spoken2" value="eng">			
				<label for="lang-spoken2">Arabic</label><br>
				<input type="checkbox" id="lang-spoken4" name="lang-spoken2" value="eng">			
				<label for="lang-spoken2">Japanese</label><br>
				<br>
			<label for="bio">Bio:</label><br><br>
			<textarea name="message" rows="10" cols="30"></textarea><br>
			<input type="submit" value="Sign Up">
		</form>
	</body>
</html>
